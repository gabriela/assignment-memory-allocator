//
// Created by gabriela on 30.12.22.
//

#ifndef MYMALLOC_ALLOCATOR_H
#define MYMALLOC_ALLOCATOR_H

#include "memory_block_utils.h"
#include <sys/mman.h>
#include "allocator_utils.h"

#define BLOCK_MIN_CAPACITY 24

int init(size_t bytes);
int deinit();
int test();

block_header* my_malloc(size_t n);
void my_free(void* addr);

#endif //MYMALLOC_ALLOCATOR_H
