//
// Created by gabriela on 02.01.23.
//

#ifndef MYMALLOC_ALLOCATOR_UTILS_H
#define MYMALLOC_ALLOCATOR_UTILS_H

#include "memory_block.h"
#include <stddef.h>

void mark_up_splits(void* start, size_t capacity);
inline size_t calculate_block_size(size_t capacity) {
    return offsetof(block_header, contents) + capacity;
}
void* split(block_header* header, size_t n);
void* unite(block_header* header);

#endif //MYMALLOC_ALLOCATOR_UTILS_H
