//
// Created by gabriela on 25.12.22.
//

#ifndef MYMALLOC_MEMORY_BLOCK_H
#define MYMALLOC_MEMORY_BLOCK_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

typedef struct { size_t bytes; } block_capacity;
typedef struct { size_t bytes; } block_size;

#pragma pack(push, 1)
struct block_header{
    struct block_header*    next;
    block_capacity capacity;
    bool           is_free;
    uint8_t        contents[];  // flexible array member
};
#pragma pack(pop)

typedef struct block_header block_header;

#endif //MYMALLOC_MEMORY_BLOCK_H
