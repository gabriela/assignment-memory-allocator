//
// Created by gabriela on 30.12.22.
//

#include <stdio.h>
#include "../include/allocator.h"

#define SPAN_START span_ptr
#define SPAN_SIZE span_size

void* span_ptr = NULL;
size_t span_size = 0;
size_t ADDITIONAL_SIZE = 320;

int init(size_t bytes) {
    SPAN_SIZE = bytes;
    void* res = mmap(SPAN_START, SPAN_SIZE, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    if (res == MAP_FAILED) {
        return 1;
    }

    block_header* header = res;
    header->is_free = true;
    header->next = NULL;
    header->capacity.bytes = SPAN_SIZE - calculate_block_size(0);

    SPAN_START = header;

    return 0;
}

int test() {
    block_header* header = SPAN_START;
    for (;header != NULL; header = header->next) {
        printf("Capacity: %zu, is free: %d\n", header->capacity.bytes, header->is_free);
    }

    printf("Now split 1\n");
    header = my_malloc(45);
    for (;header != NULL; header = header->next) {
        printf("Capacity: %zu, is free: %d\n", header->capacity.bytes, header->is_free);
    }

    printf("Now split 2\n");
    header = SPAN_START;
    my_malloc(46);
    for (;header != NULL; header = header->next) {
        printf("Capacity: %zu, is free: %d\n", header->capacity.bytes, header->is_free);
    }

    printf("Now split 3\n");
    header = SPAN_START;
    my_malloc(46);
    for (;header != NULL; header = header->next) {
        printf("Capacity: %zu, is free: %d\n", header->capacity.bytes, header->is_free);
    }

    printf("Now split 4\n");
    header = SPAN_START;
    my_malloc(46);
    for (;header != NULL; header = header->next) {
        printf("Capacity: %zu, is free: %d\n", header->capacity.bytes, header->is_free);
    }

    printf("Now free\n");
    header = SPAN_START;
    my_free(header->next);
    my_free(header->next->next);
    for (;header != NULL; header = header->next) {
        printf("Capacity: %zu, is free: %d\n", header->capacity.bytes, header->is_free);
    }

    printf("Now unite\n");
    header = SPAN_START;
    my_malloc(90);
    for (;header != NULL; header = header->next) {
        printf("Capacity: %zu, is free: %d\n", header->capacity.bytes, header->is_free);
    }

    printf("Now free\n");
    header = SPAN_START;
    my_free(header->next->next);
    for (;header != NULL; header = header->next) {
        printf("Capacity: %zu, is free: %d\n", header->capacity.bytes, header->is_free);
    }

    printf("Now extend memory\n");
    header = SPAN_START;
    my_malloc(120);
    for (;header != NULL; header = header->next) {
        printf("Capacity: %zu, is free: %d\n", header->capacity.bytes, header->is_free);
    }
}

int deinit() {
    int res = munmap(SPAN_START, SPAN_SIZE);
    if (res != 0) {
        return 1;
    }

    SPAN_SIZE = -1;
    SPAN_START = NULL;
    return 0;
}

block_header* get_last() {
    block_header* last = SPAN_START;
    for (block_header* header = last; header != NULL; header = header->next) {
        last = header;
    }
    return last;
}

int extend(size_t bytes) {
    void* res = mmap(SPAN_START+SPAN_SIZE, bytes, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    if (res == MAP_FAILED) {
        res = mmap(NULL, bytes, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    }

    if (res == MAP_FAILED) {
        return 1;
    }

    SPAN_SIZE += bytes;
    block_header* header = res;
    header->is_free = 1;
    header->next = NULL;
    header->capacity.bytes = bytes - calculate_block_size(0);

    block_header* prev = get_last();
    prev->next = header;

    return 0;

}

block_header* my_malloc(size_t n) {
    if (SPAN_START == NULL || SPAN_START == MAP_FAILED) {
        fprintf(stderr, "bro... just use init(size_t) method...\nyou are so cringe!\n");
        return NULL;
    }

    if (n < BLOCK_MIN_CAPACITY) n = BLOCK_MIN_CAPACITY;

    block_header* header = SPAN_START;
    block_header* appropriate_header = NULL;
    size_t capacity;
    for (;header != NULL; header = header->next) {
        if (!header->is_free) continue;
        capacity = header->capacity.bytes;
        while (header->next != NULL && header->next->is_free) {
            if (header + calculate_block_size(header->capacity.bytes) < header->next) break;
            header = unite(header);
            capacity = header->capacity.bytes;
        }

        if (capacity > n) {
            appropriate_header = header;
            break;
        }
    }

    if (appropriate_header == NULL) {
        extend(ADDITIONAL_SIZE);
        return my_malloc(n);
    }

    if (appropriate_header->capacity.bytes > n) appropriate_header = split(appropriate_header, n);

    appropriate_header->is_free = false;

    return appropriate_header;
}

void my_free(void* addr) {
    block_header* header = addr;
    header->is_free = true;
    while (header->next != NULL && header->next->is_free && (header + calculate_block_size(header->capacity.bytes) == header->next)) header = unite(header);
}

