//
// Created by gabriela on 25.12.22.
//

#include <stdlib.h>
#include <stdio.h>
#include "../include/allocator.h"

int main() {
    size_t bytes_to_initialize = 320;
    init(bytes_to_initialize);
    test();
    deinit();
    return 0;
}