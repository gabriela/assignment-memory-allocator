//
// Created by gabriela on 02.01.23.
//

#include <stddef.h>
#include <stdio.h>
#include "../include/allocator_utils.h"
#include "../include/allocator.h"

extern inline size_t calculate_block_size(size_t capacity);

/* this comment is draft
    block_header* mark_up_memory_block(void* start, size_t size, size_t capacity) {
    block_size block_sz = {calculate_block_size(capacity)};
    block_header* prev = NULL;
    for (void* data_ptr = start; ; data_ptr += block_sz.bytes) {
        size_t space_avail = (start + size) - data_ptr;
        block_header* block = (block_header*) data_ptr;
        if (space_avail >= block_sz.bytes) {
            block->is_free = true;
            block->capacity = (block_capacity) {capacity};
            block->next = data_ptr + block_sz.bytes;
            prev = block;
            continue;
        }
        if (space_avail < calculate_block_size(BLOCK_MIN_CAPACITY)) {
            prev->capacity.bytes += space_avail;
            prev->next = NULL;
            break;
        }
        block->is_free = true;
        block->capacity = (block_capacity) {space_avail - offsetof(block_header, contents)};
        break;
    }

    return start;
}*/

void mark_up_splits(void* start, size_t capacity) {
    block_header* first = start;
    block_header* second = start + calculate_block_size(capacity);

    if (!first || !second){
        fprintf(stderr, "Null pointers problem int mark_up_splits");
        return;
    }

    second->next = first->next;
    second->capacity.bytes = first->capacity.bytes - calculate_block_size(capacity);
    second->is_free = 1;

    first->capacity.bytes = capacity;
    first->is_free = 1;
    first->next = second;
}

void* split(block_header* header, size_t n) {
    if (!header) {
        fprintf(stderr, "NULL header");
        return NULL;
    }
    size_t remain = header->capacity.bytes - calculate_block_size(n);
    if (remain >= BLOCK_MIN_CAPACITY) mark_up_splits(header, n);
    return header;
}

void* unite(block_header* header) {
    if (!header) {
        fprintf(stderr, "NULL header");
        return NULL;
    }
    if (!(header->is_free && header->next->is_free)) return header;
    if (header + calculate_block_size(header->capacity.bytes) < header->next) return header;

    block_header* next = header->next->next;

    block_header* res = header;
    res->capacity.bytes = header->capacity.bytes + calculate_block_size(header->next->capacity.bytes);
    res->is_free = true;
    res->next = next;

    return res;
}


